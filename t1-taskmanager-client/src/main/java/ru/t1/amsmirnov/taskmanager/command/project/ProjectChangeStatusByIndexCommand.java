package ru.t1.amsmirnov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectChangeStatusByIndexResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-status-by-index";

    @NotNull
    public static final String DESCRIPTION = "Change project status by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken(), index, status);
        @NotNull final ProjectChangeStatusByIndexResponse response = getProjectEndpoint().changeProjectStatusByIndex(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
