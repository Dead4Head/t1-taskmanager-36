package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataXmlSaveFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataXmlSaveFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataXmlSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data to XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveFasterXMLRequest request = new DataXmlSaveFasterXMLRequest(getToken());
        @NotNull final DataXmlSaveFasterXMLResponse response = getDomainEndpoint().saveDataXmlFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
