package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse() {
    }

    public TaskShowByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskShowByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}