package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Project;

public final class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse() {
    }

    public ProjectUpdateByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}