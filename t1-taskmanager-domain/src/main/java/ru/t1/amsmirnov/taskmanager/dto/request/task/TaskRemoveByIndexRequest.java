package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskRemoveByIndexRequest() {
    }

    public TaskRemoveByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index
    ) {
        super(token);
        setIndex(index);
    }

    @Nullable
    public Integer getIndex() {
        return index;
    }

    public void setIndex(@Nullable final Integer index) {
        this.index = index;
    }

}