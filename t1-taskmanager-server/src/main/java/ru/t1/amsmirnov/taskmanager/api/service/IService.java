package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model) throws AbstractException;

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models) throws AbstractException;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models) throws AbstractException;

    @NotNull
    List<M> findAll() throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws AbstractException;

    @NotNull
    M findOneById(@Nullable String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@Nullable Integer index) throws AbstractException;

    void removeAll();

    void removeAll(@Nullable Collection<M> collection);

    @NotNull
    M removeOne(@Nullable M model) throws AbstractException;

    @NotNull
    M removeOneById(@Nullable String id) throws AbstractException;

    @NotNull
    M removeOneByIndex(@Nullable Integer index) throws AbstractException;

    int getSize();

    boolean existById(@NotNull String id) throws AbstractException;

}
