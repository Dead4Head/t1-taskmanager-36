package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    Project changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    Project changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws AbstractException;

}
