package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> addAll(@NotNull Collection<M> models) {
        records.addAll(models);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        removeAll();
        addAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        records.removeAll(collection);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index < 0 || index >= getSize())
            return null;
        return records.get(index);
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) {
        records.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public int getSize() {
        return records.size();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        return !(model == null);
    }

}
