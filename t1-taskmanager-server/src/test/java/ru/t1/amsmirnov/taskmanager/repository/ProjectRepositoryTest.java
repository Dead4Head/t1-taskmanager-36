package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Project> alfaProjects;

    @NotNull
    private List<Project> betaProjects;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projects = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5)
                project.setUserId(USER_ALFA_ID);
            else
                project.setUserId(USER_BETA_ID);
            projects.add(project);
            projectRepository.add(project);
        }
        alfaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        project.setName("Added project");
        project.setDescription("Added description");
        project.setUserId(USER_ALFA_ID);

        projects.add(project);
        projectRepository.add(USER_ALFA_ID, project);
        @Nullable final List<Project> actualProjectList = projectRepository.findAll();
        assertEquals(projects, actualProjectList);
    }

    @Test
    public void testRemoveAll() {
        assertNotEquals(0, projectRepository.getSize());
        projectRepository.removeAll();
        assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testRemoveAllForUser() {
        assertNotEquals(0, projectRepository.getSize(USER_ALFA_ID));
        projectRepository.removeAll(USER_ALFA_ID);
        assertEquals(0, projectRepository.getSize(USER_ALFA_ID));
    }

    @Test
    public void testExistById() {
        for (final Project project : projects) {
            assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
        }
        assertFalse(projectRepository.existById(USER_ALFA_ID, NONE_ID));
    }

    @Test
    public void testFindAll() {
        List<Project> actualAlfaProjects = projectRepository.findAll(USER_ALFA_ID);
        List<Project> actualBetaProjects = projectRepository.findAll(USER_BETA_ID);
        assertNotEquals(actualAlfaProjects, actualBetaProjects);
        assertEquals(alfaProjects, actualAlfaProjects);
        assertEquals(betaProjects, actualBetaProjects);
    }

    @Test
    public void testFindAllComparator() {
        List<Project> actualAlfaProjects = projectRepository.findAll(USER_ALFA_ID, ProjectSort.BY_NAME.getComparator());
        assertEquals(alfaProjects, actualAlfaProjects);
        assertEquals(projects, projectRepository.findAll(ProjectSort.BY_NAME.getComparator()));
    }

    @Test
    public void testFindOneById() {
        for (final Project project : projects) {
            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
        }
        assertNull(projectRepository.findOneById(USER_ALFA_ID, NONE_ID));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i <= projects.size(); i++)
            assertEquals(projects.get(0), projectRepository.findOneByIndex(0));
        for (int i = 0; i < alfaProjects.size(); i++)
            assertEquals(alfaProjects.get(i), projectRepository.findOneByIndex(USER_ALFA_ID, i));
        for (int i = 0; i < betaProjects.size(); i++)
            assertEquals(betaProjects.get(i), projectRepository.findOneByIndex(USER_BETA_ID, i));
    }

    @Test
    public void testGetSize() {
        assertEquals(projects.size(), projectRepository.getSize());
        assertEquals(alfaProjects.size(), projectRepository.getSize(USER_ALFA_ID));
        assertEquals(betaProjects.size(), projectRepository.getSize(USER_BETA_ID));
    }

    @Test
    public void testRemoveOne() {
        for (final Project project : projects) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.removeOne(project.getUserId(), project);
            assertFalse(projectRepository.existById(project.getId()));
        }
        assertNull(projectRepository.removeOne(USER_ALFA_ID, new Project()));
    }

    @Test
    public void testRemoveOneById() {
        for (final Project project : projects) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.removeOneById(project.getUserId(), project.getId());
            assertFalse(projectRepository.existById(project.getId()));
        }
        assertNull(projectRepository.removeOneById(USER_BETA_ID, NONE_ID));
    }

    @Test
    public void testRemoveOneByIndex() {
        for (final Project project : alfaProjects) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.removeOneByIndex(project.getUserId(), 0);
            assertFalse(projectRepository.existById(project.getId()));
        }
        for (final Project project : betaProjects) {
            assertTrue(projectRepository.existById(project.getId()));
            projectRepository.removeOneByIndex(project.getUserId(), 0);
            assertFalse(projectRepository.existById(project.getId()));
        }
        assertNull(projectRepository.removeOneByIndex(USER_ALFA_ID, 10));
    }

}
