package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.TaskSort;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static Project projectUserAlfa;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private List<Task> alfaTasks;

    @NotNull
    private List<Task> betaTasks;

    @NotNull
    private ITaskRepository taskRepository;

    @BeforeClass
    public static void initData() {
        projectUserAlfa = new Project();
        projectUserAlfa.setName("Test task name");
        projectUserAlfa.setDescription("Test task description");
        projectUserAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() {
        tasks = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task : " + i);
            task.setDescription("Description: " + i);
            if (i <= 5)
                task.setUserId(USER_ALFA_ID);
            else
                task.setUserId(USER_BETA_ID);
            if (i % 3 == 0)
                task.setProjectId(projectUserAlfa.getId());
            tasks.add(task);
            taskRepository.add(task);
        }
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @Test
    public void testAdd() {
        @NotNull final Task task = new Task();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER_ALFA_ID);

        tasks.add(task);
        taskRepository.add(USER_ALFA_ID, task);
        List<Task> actualTasks = taskRepository.findAll();
        assertEquals(tasks, actualTasks);
    }

    @Test
    public void testRemoveAll() {
        assertNotEquals(0, taskRepository.getSize());
        taskRepository.removeAll();
        assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testRemoveAllForUser() {
        assertNotEquals(0, taskRepository.getSize(USER_ALFA_ID));
        taskRepository.removeAll(USER_ALFA_ID);
        assertEquals(0, taskRepository.getSize(USER_ALFA_ID));
    }

    @Test
    public void testRemoveAllList() {
        assertNotEquals(0, taskRepository.getSize(USER_ALFA_ID));
        final List<Task> alfaTasks = taskRepository.findAll(USER_ALFA_ID);
        taskRepository.removeAll(alfaTasks);
        assertEquals(0, taskRepository.getSize(USER_ALFA_ID));
    }

    @Test
    public void testExistById() {
        for (final Task task : tasks) {
            assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
        }
        assertFalse(taskRepository.existById(USER_ALFA_ID, UUID.randomUUID().toString()));
    }

    @Test
    public void testFindAll() {
        assertNotEquals(taskRepository.findAll(USER_ALFA_ID), taskRepository.findAll(USER_BETA_ID));

        assertEquals(alfaTasks, taskRepository.findAll(USER_ALFA_ID));
        assertEquals(betaTasks, taskRepository.findAll(USER_BETA_ID));
    }

    @Test
    public void testFindAllComparator() {
        alfaTasks.sort(TaskSort.BY_NAME.getComparator());
        assertEquals(alfaTasks, taskRepository.findAll(USER_ALFA_ID, TaskSort.BY_NAME.getComparator()));
    }

    @Test
    public void testFindOneById() {
        for (final Task task : tasks) {
            assertEquals(task, taskRepository.findOneById(task.getUserId(), task.getId()));
        }
        assertNull(taskRepository.findOneById(NONE_ID));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < alfaTasks.size(); i++)
            assertEquals(alfaTasks.get(i), taskRepository.findOneByIndex(USER_ALFA_ID, i));
        for (int i = 0; i < betaTasks.size(); i++)
            assertEquals(betaTasks.get(i), taskRepository.findOneByIndex(USER_BETA_ID, i));

    }

    @Test
    public void testGetSize() {
        assertEquals(tasks.size(), taskRepository.getSize());
        final long alfaTasksCount = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .count();
        final long betaTasksSize = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .count();
        assertEquals(alfaTasksCount, taskRepository.getSize(USER_ALFA_ID));
        assertEquals(betaTasksSize, taskRepository.getSize(USER_BETA_ID));
    }

    @Test
    public void testRemove() {
        for (final Task task : tasks) {
            assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
            taskRepository.removeOne(task.getUserId(), task);
            assertFalse(taskRepository.existById(task.getUserId(), task.getId()));
        }
        assertNull(taskRepository.removeOne(USER_ALFA_ID, new Task()));
    }

    @Test
    public void testRemoveById() {
        for (final Task task : tasks) {
            assertTrue(taskRepository.existById(task.getId()));
            taskRepository.removeOneById(task.getUserId(), task.getId());
            assertFalse(taskRepository.existById(task.getId()));
        }
        assertNull(taskRepository.removeOneById(USER_ALFA_ID, NONE_ID));
    }

    @Test
    public void testRemoveByIndex() {
        for (final Task task : tasks) {
            if (task.getUserId().equals(USER_ALFA_ID)) {
                assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
                taskRepository.removeOneByIndex(task.getUserId(), 0);
                assertFalse(taskRepository.existById(task.getUserId(), task.getId()));
            }
            if (task.getUserId().equals(USER_BETA_ID)) {
                assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
                taskRepository.removeOneByIndex(task.getUserId(), 0);
                assertFalse(taskRepository.existById(task.getUserId(), task.getId()));
            }
        }
        assertNull(taskRepository.removeOneByIndex(USER_ALFA_ID, 0));
    }

    @Test
    public void testFindAllByProjectId() {
        final List<Task> alfaTasksWithProject = alfaTasks
                .stream()
                .filter(t -> projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> betaTasksWithProject = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID) && projectUserAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> alfaRepoTasks = taskRepository.findAllByProjectId(USER_ALFA_ID, projectUserAlfa.getId());
        final List<Task> betaRepoTasks = taskRepository.findAllByProjectId(USER_BETA_ID, projectUserAlfa.getId());
        assertNotEquals(alfaRepoTasks, betaRepoTasks);
        assertEquals(alfaTasksWithProject, alfaRepoTasks);
        assertEquals(betaTasksWithProject, betaRepoTasks);

        assertEquals(new ArrayList<>(), taskRepository.findAllByProjectId(USER_ALFA_ID, NONE_ID));
    }

}
