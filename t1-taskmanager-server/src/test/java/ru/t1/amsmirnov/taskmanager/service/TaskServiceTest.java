package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.*;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private static final String USER_ALFA_ID = randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = randomUUID().toString();

    @NotNull
    private static final Project projectAlfa = new Project();

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private List<Task> alfaTasks;

    @NotNull
    private List<Task> betaTasks;


    @BeforeClass
    public static void initData() {
        projectAlfa.setName("Alfa project");
        projectAlfa.setDescription("Alfa description");
        projectAlfa.setUserId(USER_ALFA_ID);
    }

    @Before
    public void initRepository() {
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
        tasks = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task name: " + i);
            task.setDescription("Task description: " + i);
            if (i <= 5) {
                task.setUserId(USER_ALFA_ID);
                task.setProjectId(projectAlfa.getId());
            } else
                task.setUserId(USER_BETA_ID);
            taskRepository.add(task);
            tasks.add(task);
        }
        alfaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaTasks = tasks
                .stream()
                .filter(t -> t.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @Test
    public void testChangeTaskStatusById() throws AbstractException {
        int i = 0;
        for (final Task task : tasks) {
            if (i % 2 == 0) {
                taskService.changeStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
                assertEquals(Status.IN_PROGRESS, task.getStatus());
                taskService.changeStatusById(task.getUserId(), task.getId(), null);
                assertEquals(Status.IN_PROGRESS, task.getStatus());
            }
            if (i % 3 == 0) {
                taskService.changeStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
                assertEquals(Status.COMPLETED, task.getStatus());
            }
            i++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusById_IdEmptyException_1() throws AbstractException {
        taskService.changeStatusById(NONE_STR, "", Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeTaskStatusById_IdEmptyException_2() throws AbstractException {
        taskService.changeStatusById(NONE_STR, NULL_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusById_UserEmptyException_1() throws AbstractException {
        taskService.changeStatusById("", NONE_STR, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusById_UserEmptyException_2() throws AbstractException {
        taskService.changeStatusById(NULL_STR, NONE_STR, Status.IN_PROGRESS);
    }

    @Test
    public void testChangeTaskStatusByIndex() throws AbstractException {
        for (int i = 0; i < alfaTasks.size(); i++) {
            assertNotEquals(Status.IN_PROGRESS, alfaTasks.get(i).getStatus());
            taskService.changeStatusByIndex(USER_ALFA_ID, i, Status.IN_PROGRESS);
            assertEquals(Status.IN_PROGRESS, taskService.findOneByIndex(USER_ALFA_ID, i).getStatus());
            taskService.changeStatusByIndex(USER_ALFA_ID, i, null);
            assertEquals(Status.IN_PROGRESS, taskService.findOneByIndex(USER_ALFA_ID, i).getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndex_IncorrectIndexException_1() throws AbstractException {
        taskService.changeStatusByIndex(USER_ALFA_ID, -1, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndex_IncorrectIndexException_2() throws AbstractException {
        taskService.changeStatusByIndex(USER_ALFA_ID, tasks.size(), Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeTaskStatusByIndex_IncorrectIndexException_3() throws AbstractException {
        taskService.changeStatusByIndex(USER_ALFA_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndex_UserEmptyException_1() throws AbstractException {
        taskService.changeStatusByIndex("", 0, Status.IN_PROGRESS);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeTaskStatusByIndex_UserEmptyException_2() throws AbstractException {
        taskService.changeStatusByIndex(NULL_STR, 0, Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws AbstractException {
        final String name = "TEST project";
        final String description = "Description";
        final Task task = taskService.create(USER_ALFA_ID, name, description);
        final Task actualTask = taskRepository.findOneById(task.getId());
        assertEquals(name, actualTask.getName());
        assertEquals(description, actualTask.getDescription());
        assertEquals(USER_ALFA_ID, actualTask.getUserId());
        assertEquals(taskRepository.getSize(), tasks.size() + 1);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_NameException_1() throws AbstractException {
        final String description = "Description";
        taskService.create(USER_ALFA_ID, "", description);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreate_NameException_2() throws AbstractException {
        final String description = "Description";
        taskService.create(USER_ALFA_ID, NULL_STR, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_UserEmptyException_1() throws AbstractException {
        final String description = "Description";
        taskService.create("", NONE_STR, description);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreate_UserEmptyException_2() throws AbstractException {
        final String description = "Description";
        taskService.create(NULL_STR, NONE_STR, description);
    }

    @Test
    public void testFindAllByProjectId() throws AbstractException {
        final List<Task> alfaTasksByProject = alfaTasks
                .stream()
                .filter(t -> projectAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> betaTasksByProject = betaTasks
                .stream()
                .filter(t -> projectAlfa.getId().equals(t.getProjectId()))
                .collect(Collectors.toList());
        final List<Task> alfaServTasks = taskService.findAllByProjectId(USER_ALFA_ID, projectAlfa.getId());
        final List<Task> betaServTasks = taskService.findAllByProjectId(USER_BETA_ID, projectAlfa.getId());
        assertNotEquals(alfaServTasks, betaServTasks);
        assertEquals(alfaTasksByProject, alfaServTasks);
        assertEquals(betaTasksByProject, betaServTasks);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectId_UserEmptyException_1() throws AbstractException {
        taskService.findAllByProjectId("", projectAlfa.getId());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllByProjectId_UserEmptyException_2() throws AbstractException {
        taskService.findAllByProjectId(NULL_STR, projectAlfa.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testFindAllByProjectId_ProjectEmptyException_1() throws AbstractException {
        taskService.findAllByProjectId(NONE_STR, "");
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testFindAllByProjectId_ProjectEmptyException_2() throws AbstractException {
        taskService.findAllByProjectId(NONE_STR, NULL_STR);
    }

    @Test
    public void testUpdateById() throws AbstractException {
        for (final Task task : tasks) {
            final String name = task.getName() + "TEST";
            final String description = task.getDescription() + "TEST";
            taskService.updateById(task.getUserId(), task.getId(), name, description);
            assertEquals(task.getName(), name);
            assertEquals(task.getDescription(), description);
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_1() throws AbstractException {
        taskService.updateById("", NONE_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateById_UserEmptyException_2() throws AbstractException {
        taskService.updateById(NULL_STR, NONE_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdException_1() throws AbstractException {
        taskService.updateById(NONE_STR, "", NONE_STR, NONE_STR);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateById_IdException_2() throws AbstractException {
        taskService.updateById(NONE_STR, NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_1() throws AbstractException {
        taskService.updateById(NONE_STR, NONE_STR, "", NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateById_NameEmptyException_2() throws AbstractException {
        taskService.updateById(NONE_STR, NONE_STR, NULL_STR, NONE_STR);
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        for (int i = 0; i < alfaTasks.size(); i++) {
            final String name = alfaTasks.get(i).getName() + "TEST";
            final String description = alfaTasks.get(i).getDescription() + "TEST";
            taskService.updateByIndex(alfaTasks.get(i).getUserId(), i, name, description);
            assertEquals(name, taskService.findOneById(alfaTasks.get(i).getId()).getName());
            assertEquals(description, taskService.findOneById(alfaTasks.get(i).getId()).getDescription());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndex_UserEmptyException_1() throws AbstractException {
        taskService.updateByIndex("", 0, NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndex_UserEmptyException_2() throws AbstractException {
        taskService.updateByIndex(NULL_STR, 0, NONE_STR, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndex_IndexException_1() throws AbstractException {
        taskService.updateByIndex(NONE_STR, null, NONE_STR, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndex_IndexException_2() throws AbstractException {
        taskService.updateByIndex(NONE_STR, -1, NONE_STR, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndex_IndexException_3() throws AbstractException {
        taskService.updateByIndex(NONE_STR, alfaTasks.size(), NONE_STR, NONE_STR);
    }


    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndex_NameException_1() throws AbstractException {
        taskService.updateByIndex(USER_ALFA_ID, 0, "", NONE_STR);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndex_NameException_2() throws AbstractException {
        taskService.updateByIndex(USER_ALFA_ID, 0, NULL_STR, NONE_STR);
    }

}
