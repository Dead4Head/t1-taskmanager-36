package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.api.service.IUserService;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.*;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;
import ru.t1.amsmirnov.taskmanager.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectService, taskService, propertyService);

    @NotNull
    private final List<User> users = new ArrayList<>();

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setFirstName("UserFirstName " + i);
            user.setLastName("UserLastName " + i);
            user.setMiddleName("UserMidName " + i);
            user.setEmail("user" + i + "@dot.ru");
            user.setLogin("USER" + i);
            user.setPasswordHash(user.getId());
            user.setRole(Role.USUAL);
            userRepository.add(user);
            users.add(user);

            for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                @NotNull final Project project = new Project();
                project.setName("Project name: " + i + " " + j);
                project.setDescription("Project description: " + i + " " + j);
                project.setUserId(user.getId());
                projectRepository.add(project);

                for (int a = 0; a < NUMBER_OF_ENTRIES; a++) {
                    @NotNull final Task task = new Task();
                    task.setName("Task name: " + a);
                    task.setDescription("Task description: " + a);
                    task.setUserId(user.getId());
                    task.setProjectId(project.getId());
                    taskRepository.add(task);
                }
            }
        }
    }

    @Test
    public void testCreate() throws AbstractException {
        List<User> usersRepo = userRepository.findAll();
        assertEquals(users, usersRepo);

        User user = userService.create("TEST_USER1", "TEST_PASSWORD");
        users.add(user);
        users.add(userService.create("TEST_USER2", "TEST_PASSWORD", "user@mail.com"));
        users.add(userService.create("TEST_ADMIN", "TESTADMIN", Role.ADMIN));
        usersRepo = userRepository.findAll();
        assertEquals(users.size(), usersRepo.size());
    }

    @Test(expected = EmailExistException.class)
    public void testCreate_EmailExistException_1() throws AbstractException {
        userService.create("LOGIN1", "TEST_PASSWORD", "test@test.test");
        userService.create("LOGIN2", "TEST_PASSWORD", "test@test.test");
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreate_RoleException() throws AbstractException {
        final Role role = null;
        userService.create("LOGIN1", "TEST_PASSWORD", role);
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreate_EmptyLoginException_1() throws AbstractException {
        userService.create("", "TEST_PASSWORD");
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreate_EmptyLoginException_2() throws AbstractException {
        userService.create(NULL_STR, "TEST_PASSWORD");
    }

    @Test(expected = LoginExistException.class)
    public void testCreate_LoginExistException() throws AbstractException {
        userService.create(NONE_STR, "TEST_PASSWORD");
        userService.create(NONE_STR, "TEST_PASSWORD");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreate_EmptyPasswordException_1() throws AbstractException {
        userService.create("LOGIN", "");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreate_EmptyPasswordException_2() throws AbstractException {
        userService.create("LOGIN", NULL_STR);
    }

    @Test
    public void testFindByEmail() throws AbstractException {
        for (final User user : users) {
            final User actualUser = userService.findOneByEmail(user.getEmail());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmail_EmailEmptyException_1() throws AbstractException {
        userService.findOneByEmail("");
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmail_EmailEmptyException_2() throws AbstractException {
        userService.findOneByEmail(NULL_STR);
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindByEmail_UserNotFoundException() throws AbstractException {
        userService.findOneByEmail(NONE_STR);
    }

    @Test
    public void testFindByLogin() throws AbstractException {
        for (final User user : users) {
            final User actualUser = userService.findOneByLogin(user.getLogin());
            assertEquals(user, actualUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLogin_LoginEmptyException_1() throws AbstractException {
        userService.findOneByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLogin_LoginEmptyException_2() throws AbstractException {
        userService.findOneByLogin(NULL_STR);
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindByLogin_UserNotFoundException() throws AbstractException {
        userService.findOneByLogin(NONE_STR);
    }

    @Test
    public void testIsEmailExist() throws AbstractException {
        for (final User user : users) {
            assertTrue(userService.isEmailExist(user.getEmail()));
            assertFalse(userService.isEmailExist(user.getEmail() + user.getEmail()));
        }
    }

    @Test(expected = EmailEmptyException.class)
    public void testIsEmailExists_LoginEmptyException_1() throws AbstractException {
        userService.isEmailExist("");
    }

    @Test(expected = EmailEmptyException.class)
    public void testIsEmailExists_LoginEmptyException_2() throws AbstractException {
        userService.isEmailExist(NULL_STR);
    }


    @Test
    public void testIsLoginExist() throws AbstractException {
        for (final User user : users) {
            assertTrue(userService.isLoginExist(user.getLogin()));
            assertFalse(userService.isLoginExist(user.getLogin() + user.getLogin()));
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testIsLoginExists_LoginEmptyException_1() throws AbstractException {
        userService.isLoginExist("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testIsLoginExists_LoginEmptyException_2() throws AbstractException {
        userService.isLoginExist(NULL_STR);
    }

    @Test
    public void testLockUserByLogin() throws AbstractException {
        for (final User user : users) {
            user.setLocked(false);
            assertFalse(user.isLocked());
            userService.lockUserByLogin(user.getLogin());
            assertTrue(user.isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByLogin_LoginEmptyException_1() throws AbstractException {
        userService.lockUserByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByLogin_LoginEmptyException_2() throws AbstractException {
        userService.lockUserByLogin(NULL_STR);
    }

    @Test
    public void testRemoveByEmail() throws AbstractException {
        for (final User user : users) {
            assertTrue(userRepository.existById(user.getId()));
            assertNotEquals(0, projectRepository.findAll(user.getId()).size());
            assertNotEquals(0, taskRepository.findAll(user.getId()).size());
            userService.removeByEmail(user.getEmail());
            assertFalse(userRepository.existById(user.getId()));
            assertEquals(0, projectRepository.findAll(user.getId()).size());
            assertEquals(0, taskRepository.findAll(user.getId()).size());
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveOne() throws AbstractException {
        userService.removeOne(null);
    }

    @Test
    public void testRemoveByLogin() throws AbstractException {
        for (final User user : users) {
            assertTrue(userRepository.existById(user.getId()));
            assertNotEquals(0, projectRepository.findAll(user.getId()).size());
            assertNotEquals(0, taskRepository.findAll(user.getId()).size());
            userService.removeByLogin(user.getLogin());
            assertFalse(userRepository.existById(user.getId()));
            assertEquals(0, projectRepository.findAll(user.getId()).size());
            assertEquals(0, taskRepository.findAll(user.getId()).size());
        }
    }

    @Test
    public void testSetPassword() throws AbstractException {
        for (final User user : users) {
            final String oldHash = user.getPasswordHash();
            userService.setPassword(user.getId(), "NEW_PASS" + user.getLogin());
            assertNotEquals(oldHash, user.getPasswordHash());
        }
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPassword_PasswordEmptyException_1() throws AbstractException {
        userService.setPassword(users.get(0).getId(), "");
    }


    @Test(expected = PasswordEmptyException.class)
    public void testSetPassword_PasswordEmptyException_2() throws AbstractException {
        userService.setPassword(users.get(0).getId(), NULL_STR);
    }

    @Test
    public void testUnlockUserByLogin() throws AbstractException {
        for (final User user : users) {
            user.setLocked(true);
            assertTrue(user.isLocked());
            userService.unlockUserByLogin(user.getLogin());
            assertFalse(user.isLocked());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByLogin_LoginEmptyException_1() throws AbstractException {
        userService.unlockUserByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByLogin_LoginEmptyException_2() throws AbstractException {
        userService.unlockUserByLogin(NULL_STR);
    }

    @Test(expected = LoginEmptyException.class)
    public void testLoginEmptyUnlockUserByLogin() throws AbstractException {
        userService.unlockUserByLogin("");
    }

    @Test
    public void testUserUpdateById() throws AbstractException {
        for (final User user : users) {
            final String firstName = user.getFirstName() + "TEST";
            final String lastName = user.getLastName() + "TEST";
            final String middleName = user.getMiddleName() + "TEST";
            assertNotEquals(user.getFirstName(), firstName);
            assertNotEquals(user.getLastName(), lastName);
            assertNotEquals(user.getMiddleName(), middleName);
            userService.updateUserById(user.getId(), firstName, lastName, middleName);
            assertEquals(user.getFirstName(), firstName);
            assertEquals(user.getLastName(), lastName);
            assertEquals(user.getMiddleName(), middleName);
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testUserUpdate_IdEmptyException_1() throws AbstractException {
        userService.updateUserById("", "firstName", "lastName", "middleName");
    }

    @Test(expected = IdEmptyException.class)
    public void testUserUpdate_IdEmptyException_2() throws AbstractException {
        userService.updateUserById(NULL_STR, "firstName", "lastName", "middleName");
    }

}
