package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.service.ISessionService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.repository.SessionRepository;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    final ISessionService sessionService = new SessionService(sessionRepository);

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final Date TODAY = new Date();

    @NotNull
    private final List<Session> sessions = new ArrayList<>();

    @NotNull
    private final List<Session> alfaSessions = new ArrayList<>();

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setDate(TODAY);
            if (i <= 5) {
                session.setUserId(USER_ALFA_ID);
                alfaSessions.add(session);
            } else {
                session.setUserId(USER_BETA_ID);
            }
            sessions.add(session);
            sessionRepository.add(session);
        }
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAdd_ModelNotFoundException_1() throws AbstractException {
        sessionService.add(USER_ALFA_ID, null);
    }

    @Test
    public void testFindAllUser() throws AbstractException {
        assertEquals(alfaSessions, sessionService.findAll(USER_ALFA_ID, null));
        assertEquals(sessions, sessionService.findAll());
        final Comparator<Session> comparator = null;
        assertEquals(sessions, sessionService.findAll(comparator));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_UserIdEmptyException_1() throws AbstractException {
        sessionService.findAll("", null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAll_UserIdEmptyException_2() throws AbstractException {
        sessionService.findAll(NULL_STR, null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindOneById_ModelNotFoundException_1() throws AbstractException {
        sessionService.findOneById(NONE_STR, NONE_STR);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindOneById_ModelNotFoundException_2() throws AbstractException {
        sessionService.removeAll();
        sessionService.findOneById(NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void findOneByIndex_UserIdEmptyException_1() throws AbstractException {
        sessionService.findOneByIndex(NULL_STR, 0);
    }

    @Test(expected = UserIdEmptyException.class)
    public void findOneByIndex_UserIdEmptyException_2() throws AbstractException {
        sessionService.findOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndex_IndexIncorrectException_1() throws AbstractException {
        sessionService.findOneByIndex(NONE_STR, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndex_IndexIncorrectException_2() throws AbstractException {
        sessionService.findOneByIndex(NONE_STR, -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndex_IndexIncorrectException_3() throws AbstractException {
        sessionService.findOneByIndex(NONE_STR, sessions.size());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOne_ModelNotFoundException_1() throws AbstractException {
        sessionService.removeOne(USER_ALFA_ID, null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOne_ModelNotFoundException_2() throws AbstractException {
        sessionService.removeOne(USER_ALFA_ID, new Session());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveOneById_ModelNotFoundException_2() throws AbstractException {
        sessionService.removeOneById(USER_ALFA_ID, NONE_STR);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneByIndex_IndexIncorrectException_1() throws AbstractException {
        sessionService.removeOneByIndex(USER_ALFA_ID, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneByIndex_IndexIncorrectException_2() throws AbstractException {
        sessionService.removeOneByIndex(USER_ALFA_ID, -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveOneByIndex_IndexIncorrectException_3() throws AbstractException {
        sessionService.removeOneByIndex(USER_ALFA_ID, alfaSessions.size());
    }

    @Test
    public void testAdd() throws AbstractException {
        final Session newSession = new Session();
        sessionService.add(newSession);
        sessions.add(newSession);
        assertEquals(sessions.size(), sessionService.getSize());
        assertEquals(sessions, sessionService.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAdd_ModelNotFoundException_2() throws AbstractException {
        sessionService.add(null);
    }


    @Test
    public void testAddAll() throws AbstractException {
        final List<Session> newSessions = new ArrayList<>();
        newSessions.add(new Session());
        newSessions.add(new Session());
        sessionService.addAll(newSessions);
        sessions.addAll(newSessions);
        assertEquals(sessions.size(), sessionService.getSize());
        assertEquals(sessions, sessionService.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testAddAll_ModelNotFoundException() throws AbstractException {
        sessionService.addAll(null);
    }

    @Test
    public void testSet() throws AbstractException {
        final List<Session> newSessions = new ArrayList<>();
        newSessions.add(new Session());
        newSessions.add(new Session());
        sessionService.set(newSessions);
        sessions.addAll(newSessions);
        assertEquals(newSessions.size(), sessionService.getSize());
        assertEquals(newSessions, sessionService.findAll());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testSet_ModelNotFoundException() throws AbstractException {
        sessionService.set(null);
    }

    @Test
    public void testRemoveAll() throws AbstractException {
        final List<Session> nullSessions = null;
        final List<Session> newSessions = new ArrayList<>();
        sessionService.removeAll(nullSessions);
        assertEquals(0, sessionService.getSize());

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++)
            newSessions.add(new Session());
        sessionService.addAll(newSessions);
        assertNotEquals(0, sessionService.getSize());
        sessionService.removeAll(newSessions);
        assertEquals(0, sessionService.getSize());
    }

}
