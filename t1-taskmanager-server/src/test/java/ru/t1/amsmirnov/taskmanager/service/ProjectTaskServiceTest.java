package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectTaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final List<Task> bindedTasks = new ArrayList<>();

    @NotNull
    private final List<Task> unbindedTasks = new ArrayList<>();

    @NotNull
    private Project project;

    @Before
    public void initRepository() {

        project = new Project();
        project.setName("Project");
        project.setDescription("Description");
        project.setUserId(USER_ALFA_ID);
        projectRepository.add(project);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task binded name: " + i);
            task.setDescription("Task binded description: " + i);
            task.setUserId(USER_ALFA_ID);
            task.setProjectId(project.getId());
            taskRepository.add(task);
            bindedTasks.add(task);
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task unbinded name: " + i);
            task.setDescription("Task unbinded description: " + i);
            task.setUserId(USER_ALFA_ID);
            taskRepository.add(task);
            unbindedTasks.add(task);
        }
    }

    @Test
    public void testBindTaskToProject() throws AbstractException {
        for (final Task task : unbindedTasks) {
            assertNull(task.getProjectId());
            projectTaskService.bindTaskToProject(USER_ALFA_ID, project.getId(), task.getId());
            assertEquals(project.getId(), task.getProjectId());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProject_UserIdEmptyException_1() throws AbstractException {
        projectTaskService.bindTaskToProject("", NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProject_UserIdEmptyException_2() throws AbstractException {
        projectTaskService.bindTaskToProject(NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProject_ProjectIdEmptyException_1() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, "", NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProject_ProjectIdEmptyException_2() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NULL_STR, NONE_STR);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProject_TaskIdEmptyException_1() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NONE_STR, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProject_TaskIdEmptyException_2() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NONE_STR, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBindTaskToProject_ProjectNotFoundException() throws AbstractException {
        projectTaskService.bindTaskToProject(NONE_STR, project.getId(), NONE_STR);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskToProject_TaskNotFoundException() throws AbstractException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, project.getId(), NONE_STR);
    }

    @Test
    public void testRemoveProjectById() throws AbstractException {
        assertNotEquals(0, taskRepository.findAllByProjectId(USER_ALFA_ID, project.getId()).size());
        assertTrue(projectRepository.existById(project.getId()));
        projectTaskService.removeProjectById(USER_ALFA_ID, project.getId());
        assertFalse(projectRepository.existById(project.getId()));
        assertEquals(0, taskRepository.findAllByProjectId(USER_ALFA_ID, project.getId()).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveProjectById_UserIdEmptyException_1() throws AbstractException {
        projectTaskService.removeProjectById("", NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveProjectById_UserIdEmptyException_2() throws AbstractException {
        projectTaskService.removeProjectById(NULL_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectById_ProjectIdEmptyException_1() throws AbstractException {
        projectTaskService.removeProjectById(USER_ALFA_ID, "");
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectById_ProjectIdEmptyException_2() throws AbstractException {
        projectTaskService.removeProjectById(USER_ALFA_ID, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveProjectById_ProjectNotFoundException() throws AbstractException {
        projectTaskService.removeProjectById(USER_ALFA_ID, NONE_STR);
    }

    @Test
    public void testUnbindTaskFromProject() throws AbstractException {
        for (final Task task : bindedTasks) {
            assertEquals(project.getId(), task.getProjectId());
            projectTaskService.unbindTaskFromProject(USER_ALFA_ID, project.getId(), task.getId());
            assertNull(task.getProjectId());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProject_UserIdEmptyException_1() throws AbstractException {
        projectTaskService.unbindTaskFromProject("", NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProject_UserIdEmptyException_2() throws AbstractException {
        projectTaskService.unbindTaskFromProject(NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromProject_ProjectIdEmptyException_1() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, "", NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromProject_ProjectIdEmptyException_2() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NULL_STR, NONE_STR);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskFromProject_TaskIdEmptyException_1() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NONE_STR, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskFromProject_TaskIdEmptyException_2() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NONE_STR, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUnbindTaskFromProject_ProjectNotFoundException() throws AbstractException {
        projectTaskService.unbindTaskFromProject(NONE_STR, project.getId(), NONE_STR);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskFromProject_TaskNotFoundException() throws AbstractException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, project.getId(), NONE_STR);
    }

}
